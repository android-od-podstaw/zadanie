package pl.dawidpanfil.zadanie1.screens

data class PlacesListState(
    val gyms: List<ListItemDisplayable>,
    val loading: Boolean = true,
    val error: String? = null
)