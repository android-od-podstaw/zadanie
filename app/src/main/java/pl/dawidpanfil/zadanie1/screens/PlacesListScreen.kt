package pl.dawidpanfil.zadanie1.screens

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.Divider
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.painter.BrushPainter
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.viewModel
import coil.compose.AsyncImage
import pl.dawidpanfil.zadanie1.R
import timber.log.Timber

@Composable
fun PlacesListScreen(viewModel: PlacesListViewModel = viewModel()) {

    val placesListState = viewModel.placeListStateFlow.collectAsState()

    PlaceListContent(placesListState = placesListState.value, onClick = {
        viewModel.onClickItem(it)
    })
}

@Composable
private fun PlaceListContent(placesListState: PlacesListState, onClick: (ListItemDisplayable) -> Unit) {
    Column {
        Text(
            text = stringResource(id = R.string.places_list_screen_title),
            style = MaterialTheme.typography.titleLarge,
            modifier = Modifier.padding(4.dp)
        )

        Divider()

        Spacer(modifier = Modifier.height(8.dp))

        if (placesListState.loading) {
            CircularProgressIndicator()
        } else {
            if (!placesListState.error.isNullOrBlank()) {
                Text(text = stringResource(R.string.network_error))
            }

            RenderList(gyms = placesListState.gyms,
                onClick = {
                    onClick(it)
                }
            )
        }
    }
}

@Composable
private fun RenderList(gyms: List<ListItemDisplayable>, onClick: (ListItemDisplayable) -> Unit) {
    LazyColumn {
        items(gyms.size) { index ->
            ListItem(gyms[index], onClick = {
                onClick(it)
            })
        }
    }
}

@Composable
private fun ListItem(item: ListItemDisplayable, onClick: (ListItemDisplayable) -> Unit) {
    Spacer(modifier = Modifier.width(8.dp))

    Row(verticalAlignment = Alignment.CenterVertically,
        modifier = Modifier
            .padding(4.dp)
            .clickable { onClick(item) }) {
        //@todo placeholder and error not works
        AsyncImage(
            model = item.imageUrl,
            contentDescription = "",
            modifier = Modifier
                .width(100.dp)
                .clip(RoundedCornerShape(10.dp)),
            placeholder = BrushPainter(
                Brush.linearGradient(
                    listOf(
                        MaterialTheme.colorScheme.primary, MaterialTheme.colorScheme.secondary
                    )
                )
            ),

            error = BrushPainter(
                Brush.linearGradient(
                    listOf(
                        Color.Black, Color.Red
                    )
                )
            ),
        )
        Spacer(modifier = Modifier.width(8.dp))
        Column {
            Text(item.name, style = MaterialTheme.typography.titleMedium)
            Text(item.category)
        }
    }
}