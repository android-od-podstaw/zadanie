package pl.dawidpanfil.zadanie1.screens

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch
import pl.dawidpanfil.zadanie1.Gym
import pl.dawidpanfil.zadanie1.NetworkModule
import pl.dawidpanfil.zadanie1.NetworkService
import retrofit2.HttpException
import timber.log.Timber

class PlacesListViewModel : ViewModel() {
    fun onClickItem(it: ListItemDisplayable) {
        Timber.d("kliknieto w $it")
    }

    private val networkService: NetworkService = NetworkModule().networkService

    private val mutablePlacesListStateFlow =
        MutableStateFlow(PlacesListState(gyms = emptyList(), loading = true))

    val placeListStateFlow: StateFlow<PlacesListState>
        get() = mutablePlacesListStateFlow.asStateFlow()

    init {
        viewModelScope.launch(Dispatchers.IO) {
            try {
                val gyms = networkService.getData().map { it.toDisplayable() }
                mutablePlacesListStateFlow.value =
                    PlacesListState(
                        gyms = gyms, loading = false
                    )
            } catch (e: HttpException) {
                mutablePlacesListStateFlow.value = mutablePlacesListStateFlow.value.copy(
                    loading = false,
                    error = e.code().toString()
                )
            }
        }
    }

}