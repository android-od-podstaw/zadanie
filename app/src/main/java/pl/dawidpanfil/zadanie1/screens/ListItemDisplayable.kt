package pl.dawidpanfil.zadanie1.screens

import pl.dawidpanfil.zadanie1.Gym

data class ListItemDisplayable(
    val imageUrl: String,
    val name: String,
    val category: String,
)


fun Gym.toDisplayable(): ListItemDisplayable {
    return ListItemDisplayable(
        imageUrl = imageUrl,
        name = name,
        category = category,
    )
}